#![no_std]

#[doc(inline)]
pub use sel4_config_consts as consts;

pub use sel4_config_macros::*;
